# 3D Casing For Micro Servo Motor

This is a model for a 3D printed case for the micro servo motor MG90S.
Similar Motors with the same dimensions may fit as well.
The motor is supposed to be used as a switch motor for a model railway built from bricks.

**(!) This is a draft and has neither been printed yet nor tested.**

The model was designed using [FreeCad](https://www.freecadweb.org)
The pictures below show the casing with a supporting base (greenish colour) and a lid (in brownish colour) which probably need to be printed seperately. The bluish or green wireframe motor is the model of the motor around this case has been designed around. The gear plus lever arm was not modelled.


![This is licensed under creative commons](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)

![](./isometric_view_with_motor.png)

![](./rear_view_with_motor.png)

![](./view_from_below_with_motor.png)

![View from top](./view_from_top.png)

![View from top with motor](./view_from_top_with_motor.png)

![Isometric view with support lid and motor](./isometric_view_with_lid_and_motor.png)

![](./view_from_side_with_motor.png)

